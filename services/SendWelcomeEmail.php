<?php

namespace app\services;

use app\models\CompanyInfo;
use app\models\Customer;
use splynx\models\transport\BaseMail;

/**
 * Class SendWelcomeEmail
 * @package app\services
 */
class SendWelcomeEmail
{

    /**
     * @param $customer Customer
     * @param int $companyId
     */
    public static function send($customer, $companyId = 0)
    {
        $company = (new CompanyInfo())->findById($companyId);
        if (is_null($company)) $company = new CompanyInfo();

        $mail = new BaseMail();
        $mail->type = BaseMail::TYPE_MESSAGE;
        $mail->status = BaseMail::STATUS_NEW;
        $mail->subject = 'Welcome to ' . $company->company_name . ' customer portal';
        $mail->recipient = $customer->email;
        $mail->message = 'Dear ' . $customer->name . ',<br>Welcome to ' . $company->company_name . ' customer portal. Thank you for staying with us!<br><br>Below are credentials to the customer portal, where you can find important information about your account, such as invoices, payment and usage history.<br><br>Link to portal: ' . $company->splynx_url . '/portal/<br>Your login: ' . $customer->login . '<br>Your password: ' . $customer->password . '<br><br>You can change your password in profile section of your account.<br><br>For any questions, feel free to contact our support team.<br><br>Best regards,<br>' . $company->company_name . ' team.';
        $mail->save();
    }

}