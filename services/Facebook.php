<?php


namespace app\services;


use yii\helpers\Url;

/**
 * Class Facebook
 * @package app\services
 */
class Facebook extends OAuth2
{
    /**
     * @var mixed
     */
    private $config;

    /**
     * @var string
     */
    protected $auth_url = 'https://www.facebook.com/';

    /**
     * @var string
     */
    protected $auth_method = '/dialog/oauth';

    /**
     * @var string
     */
    protected $access_token_method = '/oauth/access_token';

    /**
     * @var string
     */
    protected $api_url = 'https://graph.facebook.com/';

    /**
     * Facebook constructor.
     */
    public function __construct()
    {
        $this->config = \Yii::$app->params['facebook'];
        $this->setClientId($this->config['client_id']);
        $this->setClientSecret($this->config['client_secret']);
        $this->setVersion($this->config['version']);
        $this->setRedirectUri(Url::base('https') . '/' . $this->config['redirect_uri']);

    }

    /**
     * @param $scope
     * @param string $responseType
     * @return string
     */
    public function getAuthUrl($scope, $responseType = 'code')
    {
        return parent::getAuthUrl($scope, $responseType);
    }

    /**
     * @param $fields
     * @return mixed
     */
    public function getUser($fields) {
        return $this->get('/me', ['fields' => $fields], true);
    }



}