<?php


namespace app\services;

use phpseclib\Math\BigInteger;
use yii\base\Exception;
use yii\httpclient\Client;

/**
 * Class OAuth2
 * @package app\services
 */
abstract class OAuth2
{

    /**
     * @var BigInteger
     */
    private $client_id;

    /**
     * @var string
     */
    private $client_secret;

    /**
     * @var string
     */
    private $redirect_uri;

    /**
     * @var string
     */
    private $access_token;

    /**
     * @var string
     */
    protected $auth_url;

    /**
     * @var string
     */
    protected $auth_method;

    /**
     * @var string
     */
    protected $access_token_method;

    /**
     * @var string
     */
    protected $api_url;

    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $errors;

    /**
     * @param $scope
     * @param string $responseType
     * @return string
     */
    public function getAuthUrl($scope, $responseType = 'token') {
        return $this->auth_url . $this->version . $this->auth_method . '?client_id=' . $this->client_id . '&redirect_uri=' . $this->redirect_uri . '&scope=' . $scope . '&response_type=' . $responseType;
    }

    /**
     * @param $code
     * @return bool
     */
    public function getAccessToken($code) {
        $response = $this->get($this->access_token_method, [
            'client_id' => $this->client_id,
            'redirect_uri' => $this->redirect_uri,
            'client_secret' => $this->client_secret,
            'code' => $code
        ]);
        if (array_key_exists('error', $response))
        {
            $this->errors[] = $response['error'];
            return false;
        }
        $this->access_token = $response['access_token'];
        return true;
    }

    /**
     * @param $method
     * @param $data
     * @param bool $token
     * @return mixed
     * @throws \yii\httpclient\Exception
     */
    public function get($method, $data, $token = false) {
        if ($token) $data['access_token'] = $this->access_token;
        $client = new Client(['baseUrl' => $this->api_url]);
        $response = $client->get($this->version . $method, $data)->send();
        return json_decode($response->content, true);
    }


    /**
     * @param mixed $client_id
     */
    protected function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }

    /**
     * @param mixed $redirect_uri
     */
    protected function setRedirectUri($redirect_uri)
    {
        $this->redirect_uri = $redirect_uri;
    }

    /**
     * @param mixed $version
     */
    protected function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @param mixed $client_secret
     */
    public function setClientSecret($client_secret)
    {
        $this->client_secret = $client_secret;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

}