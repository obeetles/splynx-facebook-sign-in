Splynx Add-on Facebook Sign In 
======================

Splynx Add-on Facebook Sign In based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.4"
composer install
~~~

Install Splynx Add-on Facebook Sign In:
~~~
cd /var/www/splynx/addons/
git clone https://obeetles@bitbucket.org/obeetles/splynx-facebook-sign-in.git
cd splynx-facebook-sign-in
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-facebook-sign-in/web/ /var/www/splynx/web/facebook-sign-in
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-facebook-sign-in.addons
~~~

with following content:
~~~
location /facebook-sign-in
{
        try_files $uri $uri/ /facebook-sign-in/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Add-On Facebook-sign-in in menu "Customers".

Change Facebook API config and Redis config
~~~
config/web.php

'redis' => 
[
    'class' => 'yii\redis\Connection',
    'hostname' => 'localhost',
    'port' => 6379,
    'database' => 0,
    'password' => ''
]

'facebook' => 
[
    'client_id' => '',
    'client_secret' => '',
    'redirect_uri' => 'auth/edit',
    'version' => 'v6.0'
]
~~~