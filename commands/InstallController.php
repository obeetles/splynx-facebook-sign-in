<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public $module_status = self::MODULE_STATUS_ENABLED;

    public function getAddOnTitle()
    {
        return 'Splynx Add-on Facebook Sign In';
    }

    public function getModuleName()
    {
        return 'splynx_addon_facebook_sign_in';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerInfo',
                'actions' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\config\Mail',
                'actions' =>  ['index', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\config\CompanyInfo',
                'actions' =>  ['index'],
            ],
        ];
    }

    public function getEntryPoints()
    {
        return [
            [
                'name' => 'facebook_sign_in_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Ffacebook-sign-in',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }

    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'customers',
                'name' => 'social_id',
                'title' => 'Social ID',
                'type' => 'string',
                'is_required' => 0,
                'is_unique' => 1,
                'is_add' => 1,
                'show_in_list' => 0,
                'searchable' => 1,
                'readonly' => 1,
                'disabled' => 0
            ]
        ];
    }
}
