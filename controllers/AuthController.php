<?php

namespace app\controllers;

use app\models\Customer;
use app\models\CustomerInfo;
use app\services\Facebook;
use app\services\SendWelcomeEmail;
use yii\helpers\Url;
use yii\web\Controller;


/**
 * Class AuthController
 * @package app\controllers
 */
class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */

    public function actionIndex()
    {
        $this->getView()->title = 'Splynx Add-on Facebook Sign In';
        $facebook = new Facebook();

        return $this->render('index', [
            'authUrl' => $facebook->getAuthUrl('user_birthday,user_hometown,user_location,email,public_profile')
        ]);
    }

    public function actionEdit()
    {
        $this->getView()->title = 'Splynx Add-on Facebook Sign In';
        $customer = new Customer();
        $customerInfo = new CustomerInfo();

        $post = \Yii::$app->request->post();

        if (count($post) == 0) {
            $code = \Yii::$app->getRequest()->getQueryParam('code');
            if (!is_null($code)) {
                $facebook = new Facebook();
                $facebook->getAccessToken($code);
                $user = $facebook->getUser('id,last_name,email,first_name,birthday,address,hometown,location');
                if (!array_key_exists('id', $user)) $this->redirect(Url::home());
                else {
                    $socialId = 'fb' . $user['id'];
                    if (!is_null($id = $customer->isCustomer(["additional_attributes" => ["social_id" => $socialId]]))) $customer->login($id);

                    $customer->login = $user['email'];
                    $customer->name = $user['first_name'] . ' ' . $user['last_name'];
                    $customer->email = $user['email'];
                    $customer->street_1 = $user['hometown']['name'];
                    $customer->additional_attributes['social_id'] = $socialId;
                    $customer->password = $customer->password_repeat = '';
                    $customer->partner_id = $customer->location_id = 1;

                    $customerInfo->birthday = date_create($user['birthday'])->format('Y-m-d');
                }
            }
        } else {
            if ($customer->load($post) && $customer->validate() && $customerInfo->load($post) && $customerInfo->validate()) {
                $customer->status = 'active';
                $customer->create();
                $id = $customer->getId();
                $customerInfo->id = $id;
                $customerInfo->update();
                SendWelcomeEmail::send($customer);
                if (!is_null($id = $customer->getId())) $customer->login($id);
            } else {
                $customer->password = $customer->password_repeat = '';
            }
        }
        return $this->render('edit', compact('customer', 'customerInfo'));
    }
}
