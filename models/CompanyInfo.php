<?php

namespace app\models;

use splynx\base\BaseActiveApi;

/**
 * Class CompanyInfo
 * @package app\models
 */
class CompanyInfo extends BaseActiveApi
{

    /**
     * @var string
     */
    public $company_name;

    /**
     * @var string
     */
    public $splynx_url;

    /**
     * @var string
     */
    public static $apiUrl = 'admin/config/company-info';

}
