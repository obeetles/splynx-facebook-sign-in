<?php

namespace app\models;

use splynx\base\RedisSession;
use splynx\helpers\ApiHelper;
use splynx\models\customer\BaseCustomer;
use yii\web\Request;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends BaseCustomer
{

    /**
     * @var string
     */
    public $password_repeat;

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            [['login', 'street_1', 'password', 'password_repeat', 'additional_attributes'], 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * @param $params array
     * @return |null
     * @throws \yii\base\InvalidConfigException
     */
    public function isCustomer($params) {

        try {
            extract(ApiHelper::getInstance()->search($this->getApiUrl(), $params));
        } catch (InvalidConfigException $e) {
            return null;
        }

        if ($result == false or empty($response)) {
            return null;
        }

        if (count($response) > 0) return array_shift($response)['id'];
        else return null;

    }

    /**
     * @param $id
     */
    public function login($id) {
        session_destroy();
        $session = new RedisSession();
        $session->set('splynx_customer_id', $id);
        header('Location: ' . (new Request())->getHostInfo() . '/portal');
    }

}
