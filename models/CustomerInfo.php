<?php

namespace app\models;

use splynx\base\BaseActiveApi;

/**
 * Class CustomerInfo
 * @package app\models
 */
class CustomerInfo extends BaseActiveApi
{

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $birthday;

    /**
     * @var
     */
    public $passport;

    /**
     * @var
     */
    public $company_id;

    /**
     * @var
     */
    public $vat_id;

    /**
     * @var string
     */
    public static $apiUrl = 'admin/customers/customer-info';

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            [['birthday'], 'date', 'format' => 'php:Y-m-d'],
        ];
        return array_merge(parent::rules(), $rules);
    }

}
