<?php

return function ($params, $baseDir) {
    return [
        'defaultRoute' => 'auth',
        'components' => [
            'request' => [
                'baseUrl' => '/facebook-sign-in',
                'enableCookieValidation' => false
            ],
            'user' => [
                'identityClass' => 'splynx\models\Admin',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Ffacebook-sign-in',
                'enableAutoLogin' => false,
            ],
            'redis' => [
                'class' => 'yii\redis\Connection',
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 0,
                'password' => '8ca837edc6aec3b9'
            ],
        ],
        'params' => [
            'facebook' => [
                'client_id' => '502101770744284',
                'client_secret' => 'a73f5e706f5c750d175c614e4790bde8',
                'redirect_uri' => 'auth/edit',
                'version' => 'v6.0'
            ]
        ]
    ];
};
